/**
 * Module forcing users to connect to localhost.
 * Aborts connection attempt otherwise.
 *
 * (c) 2010 N Lum <nol888@gmail.com>
 *
 */

#include "IRCSock.h"
#include "User.h"
#include "Server.h"
#include "znc.h"

class CForceLocalConnMod : public CModule
{
public:
	MODCONSTRUCTOR(CForceLocalConnMod) {}
	virtual ~CForceLocalConnMod()
	{
	}
	
	virtual EModRet OnIRCConnecting(CIRCSock* pIRCSock)
	{
		CString forceServer = CZNC::Get().GetForceServer();
		if (forceServer.empty()) {
			GetUser()->PutStatus("No force server is specified");
			return HALT;
		} else if (GetUser()->GetCurrentServer()->GetName() != forceServer) {
			GetUser()->PutStatus("Connections to hosts other than '" + forceServer + "' disallowed by Rizon BNC TOS. Please change your server to '" + forceServer + "'");
			return HALT;
		} else {
			return CONTINUE;
		}
	}
};

MODULEDEFS(CForceLocalConnMod, "force users to connect to a specific server")
