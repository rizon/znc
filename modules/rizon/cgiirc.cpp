/*
 * CGI::IRC Spoofing Module for ZNC.
 * 
 * (c) 2009 N Lum <nol888@gmail.com
 *
 */
 
#include "znc.h"
#include "User.h"
#include <algorithm>

#ifndef HOSTNAME_IS_ALPHA
#define HOSTNAME_IS_ALPHA(c) (((*c >= 'A') && (*c <= 'Z')) || ((*c >= 'a') && (*c <= 'z')))
#endif

#ifndef HOSTNAME_IS_NUM
#define HOSTNAME_IS_NUM(c) ((*c >= '0') && (*c <= '9'))
#endif

#ifdef RIZON_US
#define CGIIRC_IP_PREFIX "192.168."
#define CGIIRC_HOSTNAME_SUFFIX ".RizonBNC.us.rizon.net"
#elif RIZON_EU
#define CGIIRC_IP_PREFIX "172.16."
#define CGIIRC_HOSTNAME_SUFFIX ".RizonBNC.eu.rizon.net"
#else
#error When compiling the cgiirc module one of RIZON_US or RIZON_EU must be defined.
#endif

class CCgiIrcMod : public CGlobalModule {
public:
	GLOBALMODCONSTRUCTOR(CCgiIrcMod) {}
	
	virtual ~CCgiIrcMod() {}
	
	virtual bool OnLoad(const CString& sArgs, CString& sMessage) {
		CString quad3 = GetNV("**SPECIAL-QUAD3**");
		CString quad4 = GetNV("**SPECIAL-QUAD4**");
		
		if(quad3.empty()) {
			SetNV("**SPECIAL-QUAD3**", "0");
			quad3 = "0";
		}
		if(quad4.empty()) {
			SetNV("**SPECIAL-QUAD4**", "0");
			quad4 = "0";
		}
	
		m_iIpQuad3 = quad3.ToUInt();
		m_iIpQuad4 = quad4.ToUInt();

		return true;
	}
	
	virtual EModRet OnIRCRegistration(CString& sPass, CString& sNick, CString& sIdent, CString& sRealName) {
		CString fake = GetFake(m_pUser->GetUserName());
		CString pass = CZNC::Get().GetWebircPassword();
		if (pass.empty()) {
			GetUser()->PutStatus("No webirc password is configured");
			return HALT;
		}

		PutIRC("WEBIRC " + pass + " ZNC " + CleanHostname(m_pUser->GetCleanUserName()) + CGIIRC_HOSTNAME_SUFFIX + " " + fake);

		return CONTINUE;
	}

private:
	u_int				m_iIpQuad3;
	u_int				m_iIpQuad4;
	
	CString GetFake(const CString& name) {
		CString stored = GetNV(name);
		
		if(stored.empty()) {
			stored = IncIp();
			SetNV(name, stored);
		}
		
		return CGIIRC_IP_PREFIX + stored;
	}

	CString IncIp() {
		// Save the old values, incrementing the quad4 count.
		u_int quad3 = m_iIpQuad3;
		u_int quad4 = m_iIpQuad4++;
		
		if(m_iIpQuad4 > 255) {
			// Set the IP from 192.168.1.255 to
			//                         2.0
			m_iIpQuad4 = 0;
			m_iIpQuad3++;
		}
		
		// Save the new counts.
		SetNV("**SPECIAL-QUAD3**", CString(m_iIpQuad3));
		SetNV("**SPECIAL-QUAD4**", CString(m_iIpQuad4));
		
		// Return the new IP substring.
		return CString(quad3) + "." + CString(quad4);
	}

	CString CleanHostname(const CString unclean) {
		CString clean;
		const char* chararray = unclean.c_str();
		
		while(*chararray) {
			if(HOSTNAME_IS_ALPHA(chararray) || HOSTNAME_IS_NUM(chararray) || (*chararray == '-')) {
				clean += CString(*chararray);
			} else {
				clean += "-";
			}
			
			chararray++;
		}
		
		return clean;
	}
};

GLOBALMODULEDEFS(CCgiIrcMod, "Provide dynamic IP spoofing to assist with user accountability.");
